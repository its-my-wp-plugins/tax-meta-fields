<?php
/**
 * Havens tax meta fields
 *
 * Plugin Name: Havens tax meta fields
 * Plugin URI:  https://havens-sita.net/
 * Description: Add meta fields to the tax/term settings
 * Version:     1.0
 * Author:      Haven
 * Author URI:  https://havens-sita.net/
 * Text Domain: hvn-tax-meta-fields
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Invalid request.' );
}

define( 'HVN_TAX_META_FIELDS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'HVN_TAX_META_FIELDS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

include_once HVN_TAX_META_FIELDS_PLUGIN_DIR . 'class-hvn-meta-fields.php';