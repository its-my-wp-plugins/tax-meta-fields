<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( 'Invalid request.' );
}

class Hvn_tax_meta_fields {

	private $taxonomies = array('category', 'post_tag');

	private $term_meta_fields = array(
		'title' => array(
			'label' => 'Meta title',
			'name' => 'hvn-meta-title',
			'field_type' => 'input'
		),
		'description' => array(
			'label' => 'Meta description',
			'name' => 'hvn-meta-description',
			'field_type' => 'textarea'
		),
		'keywords' => array(
			'label' => 'Meta keywords',
			'name' => 'hvn-meta-keywords',
			'field_type' => 'input'
		)
	);

	public function __construct() {

		add_action('after_setup_theme', function() {
			add_theme_support( 'title-tag' );
		});
		
		add_action('init', array($this, 'init'));
	}

	public function init(){

		$this->taxonomies = apply_filters( 'hvn_taxonomies_to_add_meta_fields', $this->taxonomies);
		$this->term_meta_fields = apply_filters( 'hvn_term_meta_fields', $this->term_meta_fields);

		if(count($this->taxonomies)):

			foreach($this->taxonomies as $taxonomy):

				add_action( $taxonomy . '_add_form_fields', array($this, 'hvn_add_term_meta_fields') );
				add_action( 'created_' . $taxonomy, array($this, 'hvn_save_term_meta_fields') );

				add_action( $taxonomy . '_edit_form_fields', array($this, 'hvn_term_meta_fields'), 10, 2 );
				add_action( 'edited_' . $taxonomy, array($this, 'hvn_save_term_meta_fields') );

			endforeach;

		endif;


		add_filter( 'pre_get_document_title', array($this, 'hvn_filter_meta_title') );
		add_action( 'wp_head', array( $this, 'hvn_display_meta_description' ), 1 );
		add_action( 'wp_head', array( $this, 'hvn_display_meta_keywords' ), 1 );

	}

	public function hvn_filter_meta_title($title) {

		if( is_category() || is_tag() || is_tax() ){

			$term = get_queried_object();
			$meta_title = get_term_meta( $term->term_id, $this->term_meta_fields['title']['name'], 1 );
			$title = $meta_title && !empty($meta_title) ? $meta_title : $title;

		}

		return $title;
	}

	public function hvn_display_meta_description($title) {

		$desc = '';

		if( ( is_category() || is_tag() || is_tax() ) ){

			$term = get_queried_object();
			$desc = get_term_meta( $term->term_id, $this->term_meta_fields['description']['name'], 1 );
			$desc = str_replace( [ "\n", "\r" ], ' ', $desc );
			$desc = preg_replace( '~\[[^\]]+\](?!\()~', '', $desc );

		}
		
		echo $desc ? sprintf( "<meta name=\"description\" content=\"%s\" />\n", esc_attr( trim( $desc ) ) ): '';

	}

	public function hvn_display_meta_keywords($title) {

		$keywords = '';

		if( ( is_category() || is_tag() || is_tax() ) ){

			$term = get_queried_object();
			$keywords = get_term_meta( $term->term_id, $this->term_meta_fields['keywords']['name'], 1 );

		}
		
		echo $keywords ? '<meta name="keywords" content="'. esc_attr( $keywords ) .'" />' . "\n" : '';

	}

	public function hvn_add_term_meta_fields( $taxonomy ) {

		if(count($this->term_meta_fields)):

			echo '<div class="form-field">';

			foreach($this->term_meta_fields as $field):

			echo '<label for="'. $field['name'] .'">'. $field['label'] .'</label>';
			switch ($field['field_type']) {
				case 'input':
					echo '<input type="text" name="'. $field['name'] .'" id="'. $field['name'] .'" />';
					break;
				case 'textarea':
					echo '<textarea name="'. $field['name'] .'" id="'. $field['name'] .'" /></textarea>';
					break;
			}
			
			endforeach;

			echo '</div>';

		endif;
	
	}

	public function hvn_term_meta_fields( $term, $taxonomy ) {

		if(count($this->term_meta_fields)):

			foreach($this->term_meta_fields as $field):

				$value = get_term_meta( $term->term_id, $field['name'], true );
		
				echo '<tr class="form-field">
						<th><label for="'. $field['name'] .'">'. $field['label'] .'</label></th>
						<td>';
				switch ($field['field_type']) {
					case 'input':
						echo '<input type="text" name="'. $field['name'] .'" id="'. $field['name'] .'" value="' . esc_attr( $value ) .'" />';
						break;
					case 'textarea':
						echo '<textarea name="'. $field['name'] .'" id="'. $field['name'] .'" />' . esc_attr( $value ) .'</textarea>';
						break;
				}
				echo '</td>
				</tr>';

			endforeach;
			
		endif;
	
	}

	public function hvn_save_term_meta_fields($term_id) {
		if(count($this->term_meta_fields)):

			foreach($this->term_meta_fields as $field):

				update_term_meta(
					$term_id,
					$field['name'],
					sanitize_text_field( $_POST[ $field['name'] ] )
				);

			endforeach;
			
		endif;
	}
}

new Hvn_tax_meta_fields();